#include "dlist.h"

extern void addItemToList(int n, DLList* list);
extern void removeAll(int n, DLList* list);

DLLElement::DLLElement(void *itemPtr, int sortKey)
{
	key = sortKey;
	item = itemPtr;

	next = NULL;
	prev = NULL;
}

void DLList::HW1Test()
{
	DLList* list = new DLList();
	addItemToList(10, list);
	removeAll(10, list);
}
DLList::DLList() 
{
	first = NULL;
	last = NULL;
}
DLList::~DLList()
{
	DLLElement* nextElem = first;
	while(true)
	{
		if (nextElem == NULL)
			return;
		delete nextElem;
		nextElem = nextElem->next;
	}
}

void DLList::Prepend(void *item)
{
	DLLElement* newListElem;
	
	if (!IsEmpty())
	{
		newListElem = new DLLElement(item, NORMAL_PRIORITY_KEY);

		first = newListElem;
		last = newListElem;
	}
	else
	{
		newListElem = new DLLElement(item, first->key -1 );
		newListElem->next = first;
		first->prev = newListElem;

		first = newListElem;
	}
}
void DLList::Append(void *item) 
{
	DLLElement* newListElem;

	if (!IsEmpty()){

		newListElem = new DLLElement(item, NORMAL_PRIORITY_KEY);
		first = newListElem;
		last = newListElem;
	}
	else
	{
		newListElem = new DLLElement(item, last->key + 1);
		newListElem->prev = last;
		last->next = newListElem;
	}
	last = newListElem;
} 
void* DLList::Remove(int* keyPtr) {
	if (!IsEmpty())
		return NULL;

	DLLElement* removeNode = first;
	*keyPtr = removeNode->key;
	first = first->next;
	return removeNode->item;
}  

bool DLList::IsEmpty() {
	if(first == NULL)
		return false;
	else
		return true;
}             

void DLList::SortedInsert(void *item, int sortKey) {
	DLLElement* node = first;
	DLLElement* newListElem = new DLLElement(item, sortKey);

	if (node == NULL) // list is empty
	{
		first = newListElem;
		last = newListElem;
		return;
	}
	else if (sortKey <= node->key) //insert at first
	{
		first = newListElem;
		first->next = node;
		node->prev = first;
		return;
	}
	else
	{
		for (DLLElement* curNode = node; curNode != NULL; curNode = curNode->next)
		{
			if (sortKey <= curNode->key)
			{
				DLLElement* prevNode = curNode->prev;
				
				prevNode->next = newListElem;
				curNode->prev = newListElem;
				newListElem->prev = prevNode;//->next;
				newListElem->next = curNode;

				
				return;
			}
		}

		last->next = newListElem;
		newListElem->prev = last;
		last = newListElem;
	}
}
void* DLList::SortedRemove(int sortKey) {
	DLLElement* node = first;

	if (node == NULL)
		return NULL;

	do{
		if (node->key == sortKey)
		{
			//remove
			if (node->prev == NULL) // node is first node
			{
				first = node->next;
				first->prev = NULL;
			}
			else if (node == last) // node is last node.
			{
				last = node->prev;
				last->next = NULL;
			}
			else
			{
				node->prev->next = node->next;
				node->next->prev = node->prev;
			}

			return node;
		}
		
	} while ((node = node->next) != NULL);

	return NULL; 
	
} 
