#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "dlist.h"

void addItemToList(int n, DLList* list)
{
	srand(time(NULL));
	int gap = 'z' - 'a';
	for(int i = 0 ; i < n ; i++)
	{
		int key = rand() % 11 + (NORMAL_PRIORITY_KEY -10); 
		char* item = new char(rand() % gap + 'a' );
		list->SortedInsert((void*)item,key);
	}
}
void removeAll(int n, DLList* list)
{
	int key;
	printf("HW1Test - removeAll func() -----\n ");
	for (int i = 0; i < n; i++)
	{
		void* item = list->Remove(&key);
		printf(" key=%d item=%c \n", key, *((char*)item));
	}
}
